FROM openjdk:11
EXPOSE 8080
COPY target/Library-0.0.1-SNAPSHOT.jar Library-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","Library-0.0.1-SNAPSHOT.jar"]