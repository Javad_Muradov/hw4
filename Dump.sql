--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: added_books; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.added_books (
    rentid bigint NOT NULL,
    added_book_id bigint,
    owner_user_id bigint
);


ALTER TABLE public.added_books OWNER TO postgres;

--
-- Name: book_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.book_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.book_sequence OWNER TO postgres;

--
-- Name: books; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.books (
    book_id bigint NOT NULL,
    author_name character varying(255),
    author_surname character varying(255),
    available boolean DEFAULT true,
    book_name character varying(255),
    category character varying(255)
);


ALTER TABLE public.books OWNER TO postgres;

--
-- Name: rent_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rent_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_sequence OWNER TO postgres;

--
-- Name: user_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_sequence OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    user_id bigint NOT NULL,
    email character varying(255) NOT NULL,
    firstname character varying(255),
    lastname character varying(255),
    password character varying(255) NOT NULL,
    token character varying(255)
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Data for Name: added_books; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.added_books (rentid, added_book_id, owner_user_id) FROM stdin;
\.


--
-- Data for Name: books; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.books (book_id, author_name, author_surname, available, book_name, category) FROM stdin;
7	Nietzsche	Friedrich	t	Beyond_Good_and_Evil	Philosophy
8	Kant	Immanuel	t	Critique_of_Pure_Reason	Philosophy
1	Miguel	Cervantes	t	Don_Quixote	Fantasy_Fiction 
2	Patrick	Rothfuss	t	The_Name_of_the_Wind	Fantasy_Fiction 
3	Shakespeare	William	t	Hamlet	Drama
5	Flynn	MiGillian	t	Gone_Girl	Detective
6	Chandler	Raymond	t	The_Big_Sleep	Detective
4	Arthur	Miller	t	The_Crucible	Drama
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (user_id, email, firstname, lastname, password, token) FROM stdin;
\.


--
-- Name: book_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.book_sequence', 8, true);


--
-- Name: rent_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rent_sequence', 13, true);


--
-- Name: user_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_sequence', 7, true);


--
-- Name: added_books added_books_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.added_books
    ADD CONSTRAINT added_books_pkey PRIMARY KEY (rentid);


--
-- Name: books books_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.books
    ADD CONSTRAINT books_pkey PRIMARY KEY (book_id);


--
-- Name: users uk_6dotkott2kjsp8vw4d0m25fb7; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT uk_6dotkott2kjsp8vw4d0m25fb7 UNIQUE (email);


--
-- Name: books uk_hvrbuk2451gcnwthweo2r04ki; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.books
    ADD CONSTRAINT uk_hvrbuk2451gcnwthweo2r04ki UNIQUE (book_name);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);


--
-- Name: added_books fk4ur8o561eych13bw86ssbccl1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.added_books
    ADD CONSTRAINT fk4ur8o561eych13bw86ssbccl1 FOREIGN KEY (owner_user_id) REFERENCES public.users(user_id);


--
-- Name: added_books fkojdwc5u8dknvo13x0q6un0mj; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.added_books
    ADD CONSTRAINT fkojdwc5u8dknvo13x0q6un0mj FOREIGN KEY (added_book_id) REFERENCES public.books(book_id);


--
-- PostgreSQL database dump complete
--

