package edu.ada.service.Library.service;

import edu.ada.service.Library.model.dto.BookModel;
import edu.ada.service.Library.model.entity.BookEntity;
import org.springframework.stereotype.Service;

import java.util.List;

public interface BookService {

    BookEntity addNewBook(BookModel bookname);

    List<BookEntity> searchByCategory(String category);
    List<BookEntity> searchByAuthorName(String authorName);
    List<BookEntity> searchByBookName(String bookName);
    List<BookEntity> searchByCategoryAndAuthorName(String category,String authorName);
    List<BookEntity> searchByAvailable(Boolean available);
    List<BookEntity> showAllBooks();
    BookModel searchById(Long bookId);
}
