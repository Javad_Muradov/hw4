package edu.ada.service.Library.service.impl;

import edu.ada.service.Library.model.dto.BookModel;
import edu.ada.service.Library.model.dto.CommentModel;
import edu.ada.service.Library.model.entity.BookEntity;
import edu.ada.service.Library.model.entity.CommentEntity;
import edu.ada.service.Library.repository.BookRepo;
import edu.ada.service.Library.service.BookService;
import edu.ada.service.Library.service.CommentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookServiceimpl implements BookService {

    protected static Logger log = LoggerFactory.getLogger(AuthServiceImpl.class);

    @Autowired
    BookRepo bookRepo;

    @Autowired
    private CommentService commentService;


    // Belongs to new Assignment
    @Override
    public BookModel searchById(Long bookId) {
        BookEntity output=bookRepo.findByBookId(bookId);
        if(output!=null){
            BookModel bookModel= new BookModel(output);
            bookModel.setComments(commentService.getCommentByBookExtId(bookId));
            return bookModel;
        }else {
            return null;
        }
    }

    /*************************************************************************/
    @Override
    public BookEntity addNewBook(BookModel bookname) {
        try {
            BookEntity bookEntity= new BookEntity(bookname);
            bookRepo.save(bookEntity);
            return bookEntity;

        } catch (Exception e){
            log.error(e.getMessage());
            return null;
        }
    }

    @Override
    public List<BookEntity> showAllBooks() {
        List<BookEntity> output;
        output=bookRepo.findAll();
        return output;
    }

    @Override
    public List<BookEntity> searchByCategory(String category) {
        List<BookEntity> output;
        output=bookRepo.findAllByCategory(category);
        if(output!=null){
            return output;
        }else {
            return null;

        }
    }

    @Override
    public List<BookEntity> searchByAuthorName(String authorName) {
        List<BookEntity> output;
        output=bookRepo.findAllByAuthorName(authorName);
        if(output!=null){
            return output;
        }else {
            return null;

        }
    }

    @Override
    public List<BookEntity> searchByBookName(String bookName) {
        List<BookEntity> output;
        output=bookRepo.findAllByBookName(bookName);
        if(output!=null){
            return output;
        }else {
            return null;

        }
    }

    @Override
    public List<BookEntity> searchByCategoryAndAuthorName(String category, String authorName) {
        List<BookEntity> output;
        output=bookRepo.findAllByCategoryAndAuthorName(category,authorName);
        if(output!=null){
            return output;
        }else {
            return null;

        }
    }

    @Override
    public List<BookEntity> searchByAvailable(Boolean available) {
        List<BookEntity> output;
        output=bookRepo.findAllByAvailableTrue();
        if(output!=null){
            return output;
        }else {
            return null;

        }
    }


}
