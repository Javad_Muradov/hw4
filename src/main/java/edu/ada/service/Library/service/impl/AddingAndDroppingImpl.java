package edu.ada.service.Library.service.impl;

import edu.ada.service.Library.model.entity.AddedBooksEntity;
import edu.ada.service.Library.model.entity.BookEntity;
import edu.ada.service.Library.model.entity.UserEntity;
import edu.ada.service.Library.repository.AddedBooksRepo;
import edu.ada.service.Library.repository.BookRepo;
import edu.ada.service.Library.repository.UserRepo;
import edu.ada.service.Library.service.AddingAndDropping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddingAndDroppingImpl implements AddingAndDropping {

    protected static Logger log = LoggerFactory.getLogger(AuthServiceImpl.class);

    @Autowired
    BookRepo bookRepo;

    @Autowired
    UserRepo userRepo;

    @Autowired
    AddedBooksRepo addedBooksRepo;

    // In order to Add the book to profile USER Need to provide
    // his/ger token that was printed on screen when user logged in
    //User also needs to provide book_id that he want to add/drop in URL section.(Path variable)
    @Override
    public AddedBooksEntity addBook(Long book_id, String token) {
        UserEntity userEntity=userRepo.findByToken(token);
        BookEntity bookEntity=bookRepo.findByBookId(book_id);
        if(userEntity.getToken().equals(token) && bookEntity.getAvailable()){
            try {
                AddedBooksEntity addedBooksEntity= new AddedBooksEntity(userEntity,bookEntity);
                addedBooksRepo.save(addedBooksEntity);
                bookEntity.setAvailable(false);
                bookRepo.save(bookEntity);
                return addedBooksEntity;

            } catch (Exception e){
                log.error(e.getMessage());
                return null;
            }
        }else {
            return null;
        }

    }

    // In order to Drop the book to profile USER Need to provide
    // his/ger token that was printed on screen when user logged in
    //User also needs to provide book_id that he want to add/drop in URL section.(Path variable)
    @Override
    public AddedBooksEntity dropBook(Long book_id, String token) {
        UserEntity userEntity=userRepo.findByToken(token);
        BookEntity bookEntity=bookRepo.findByBookId(book_id);
        AddedBooksEntity addedBooksEntity= addedBooksRepo.findFirstByAdded(bookEntity);


        if(userEntity.getUser_id()==addedBooksEntity.getOwner().getUser_id()){
                addedBooksRepo.delete(addedBooksEntity);
                bookEntity.setAvailable(true);
                bookRepo.save(bookEntity);
                return addedBooksEntity;
        }else {

            return null;
        }

    }

    @Override
    public List<AddedBooksEntity> showBooksOfUser(String token) {
        UserEntity userEntity=userRepo.findByToken(token);
        if(userEntity.getUser_id()>0){
            List <AddedBooksEntity> addedBooksEntities=addedBooksRepo.findAllByOwner(userEntity);
            return addedBooksEntities;
        }else {
            return null;
        }

    }


}
