package edu.ada.service.Library.service;

public interface HashService {
     String hash(String string);
}
