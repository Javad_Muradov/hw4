package edu.ada.service.Library.service.impl;

import edu.ada.service.Library.service.Tokengenerator;
import org.springframework.stereotype.Service;

@Service
public class TokenGeneratorimpl implements Tokengenerator {

        @Override
        public  String getToken(int n)
        {

            // chose a Character random from this String
            String token = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                    + "0123456789"
                    + "abcdefghijklmnopqrstuvxyz";

            // create StringBuffer size of AlphaNumericString
            StringBuilder sb = new StringBuilder(n);

            for (int i = 0; i < n; i++) {

                // generate a random number between
                // 0 to AlphaNumericString variable length
                int index
                        = (int)(token.length()
                        * Math.random());

                // add Character one by one in end of sb
                sb.append(token
                        .charAt(index));
            }

            return sb.toString();
        }

    }

