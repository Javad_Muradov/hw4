package edu.ada.service.Library.service.impl;

import edu.ada.service.Library.model.dto.CommentModel;
import edu.ada.service.Library.model.entity.CommentEntity;
import edu.ada.service.Library.repository.CommentRepo;
import edu.ada.service.Library.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepo commentRepo;

    @Override
    public List<CommentModel> getCommentByBookExtId(Long bookExtId) {

        List<CommentModel> comments=new ArrayList<>(1);
        List<CommentEntity> result=commentRepo.findAllByBookExtId(bookExtId);

        if(result!=null){

            if (result.size()>0){
                result.forEach((commentEntity -> {
                    comments.add(new CommentModel(commentEntity));
                    System.out.println(comments.get(comments.size()-1).getReplies());

                }));
            }

            return comments;

        }else {

            return comments;

        }
    }

    // User need to provide his name, id of the book he wanted to give feedback and his feedback itself
    @Override
    public void addComment(String authorName, Long bookExtId, String content) {
        CommentEntity newComment = new CommentEntity(authorName,bookExtId,content);
        commentRepo.save(newComment);

    }

    // User need to provide his name, id of the comment he wanted to reply and his reply itself
    @Override
    public void replyToComment(String authorName, String cmmntID, String content) {
        Optional<CommentEntity> comment= commentRepo.findById(cmmntID);
        List <CommentEntity> reply = new ArrayList(1);
        if(comment.get().getReplies()!=null){
            reply.addAll(comment.get().getReplies());
        }

        reply.add(new CommentEntity(authorName,comment.get().getBookExtId(), content));
        comment.get().setReplies(reply);
        commentRepo.save(comment.get());

    }


}
