package edu.ada.service.Library.service.impl;

import edu.ada.service.Library.model.dto.RegistrationModel;
import edu.ada.service.Library.model.entity.UserEntity;
import edu.ada.service.Library.repository.UserRepo;
import edu.ada.service.Library.service.AuthService;
import edu.ada.service.Library.service.HashService;
import edu.ada.service.Library.service.Tokengenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    protected static Logger log = LoggerFactory.getLogger(AuthServiceImpl.class);


    @Autowired
    private UserRepo userRepo;

    @Autowired
    private Tokengenerator tokengenerator;

    @Autowired
    private HashService hashService;

    @Override
    public UserEntity registration(RegistrationModel registrationModel) {
        try {
            UserEntity user= new UserEntity(registrationModel);
            user.setToken(tokengenerator.getToken(30));
            user.setPassword(hashService.hash(user.getPassword()));
            userRepo.save(user);
            return user;

        } catch (Exception e){
            log.error(e.getMessage());
            return null;
        }

    }

    @Override
    public UserEntity login(String email, String password) {

       UserEntity user;
       user=userRepo.findFirstByEmail(email);
       if(user!=null && user.getUser_id()>0){
           password=hashService.hash(password);
           user=userRepo.findFirstByEmailAndPassword(email, password);
           if (user!=null && user.getUser_id()>0){
               return user;
           }else {
               return null;
           }
       } else {
           return null;
       }
    }

    @Override
    public UserEntity getUserByToken(String token) {
        UserEntity user=userRepo.findByToken(token);
        if(user!=null){
            return user;
        }else {
            return null;

        }    }
}
