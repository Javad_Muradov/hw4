package edu.ada.service.Library.service;

import edu.ada.service.Library.model.entity.AddedBooksEntity;

import java.util.List;

public interface AddingAndDropping {

    AddedBooksEntity addBook(Long book_id , String token);

    AddedBooksEntity dropBook(Long book_id , String token);

    List<AddedBooksEntity> showBooksOfUser(String token);
}
