package edu.ada.service.Library.service;

import edu.ada.service.Library.model.dto.CommentModel;
import edu.ada.service.Library.model.entity.CommentEntity;

import java.util.List;

public interface CommentService {
    List<CommentModel> getCommentByBookExtId(Long bookExtId);
    void addComment(String authorName, Long bookExtId, String content);
    void replyToComment(String authorName,String cmmntID,String content);

}
