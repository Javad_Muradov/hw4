package edu.ada.service.Library.repository;

import edu.ada.service.Library.model.entity.CommentEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepo extends MongoRepository<CommentEntity,String> {
    List<CommentEntity> findAllByBookExtId(Long bookExtId);

}
