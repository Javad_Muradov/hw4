package edu.ada.service.Library.repository;

import edu.ada.service.Library.model.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepo extends JpaRepository<BookEntity,Long> {

    List<BookEntity> findAllByAuthorName(String authorName);

    List<BookEntity> findAllByBookName(String bookname);

    List<BookEntity> findAllByCategory(String category);

    List<BookEntity> findAllByCategoryAndAuthorName(String category,String authorName);

    List<BookEntity> findAllByAvailableTrue();

    BookEntity findByBookId(Long bookId);



}
