package edu.ada.service.Library.repository;

import edu.ada.service.Library.model.entity.AddedBooksEntity;
import edu.ada.service.Library.model.entity.BookEntity;
import edu.ada.service.Library.model.entity.UserEntity;
import edu.ada.service.Library.service.impl.AuthServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddedBooksRepo extends JpaRepository<AddedBooksEntity,Long> {

    AddedBooksEntity findFirstByAdded(BookEntity bookEntity);

    List<AddedBooksEntity> findAllByOwner(UserEntity userEntity);
}
