package edu.ada.service.Library.controller.impl;

import edu.ada.service.Library.controller.ProfileWS;
import edu.ada.service.Library.model.entity.AddedBooksEntity;
import edu.ada.service.Library.service.AddingAndDropping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(value = "/myprofile")
public class ProfileWSimpl implements ProfileWS {

    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSimpl.class);

    @Autowired
    AddingAndDropping addingAndDropping;

    @Override
    @RequestMapping(value="/add/{book_id}",method= RequestMethod.GET)
    public ResponseEntity addBook(
            @PathVariable Long book_id,
            @RequestHeader String token) {

             AddedBooksEntity addedBooksEntity=addingAndDropping.addBook(book_id,token);
            if(addedBooksEntity==null){
                return  new ResponseEntity("Sorry Book that you want to add is Not available",
                        HttpStatus.UNPROCESSABLE_ENTITY);
            }else {
                return  new ResponseEntity(
                        "Book with Name: "+addedBooksEntity.getAdded().getBookName()+"Added to user:"
                        +addedBooksEntity.getOwner().getFirstname()
                        , HttpStatus.CREATED);
            }



    }

    @Override
    @RequestMapping(value="/drop/{book_id}",method= RequestMethod.GET)
    public ResponseEntity dropBook(
            @PathVariable Long book_id,
            @RequestHeader String token) {
        AddedBooksEntity addedBooksEntity=addingAndDropping.dropBook(book_id,token);
        if(addedBooksEntity==null){
            return  new ResponseEntity("Sorry I cannot delete the book Make." +
                    "Are you sure that you own this book?",
                    HttpStatus.UNPROCESSABLE_ENTITY);
        }else {
            return  new ResponseEntity(
                    "Book with Name: "+addedBooksEntity.getAdded().getBookName()+
                            "Deleted from User:"
                            +addedBooksEntity.getOwner().getFirstname()
                    , HttpStatus.CREATED);
        }
    }

    //In order to see the books User added to his/her profile --> Adding token to header part of request is enough
    @Override
    @RequestMapping(value="",method= RequestMethod.GET)
    public ResponseEntity showBooksOfUser(@RequestHeader String token) {
        List<AddedBooksEntity> myBooks = addingAndDropping.showBooksOfUser(token);
        if(myBooks.isEmpty()){
            return new ResponseEntity(
                    "It seems you have not take any books from our library",
                    HttpStatus.NOT_FOUND);

        }else {
                return new ResponseEntity(myBooks,HttpStatus.OK);
        }
    }
}
