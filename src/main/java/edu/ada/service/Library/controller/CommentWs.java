package edu.ada.service.Library.controller;

import org.springframework.http.ResponseEntity;

public interface CommentWs {
    ResponseEntity addComment(String token, Long bookExtId, String content);
    ResponseEntity replyToComment(String token, String cmmntID, String content);
}
