package edu.ada.service.Library.controller;

import edu.ada.service.Library.model.entity.AddedBooksEntity;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ProfileWS {
    ResponseEntity addBook(Long book_id , String token);
    ResponseEntity dropBook(Long book_id , String token);
    ResponseEntity showBooksOfUser(String token);
}
