package edu.ada.service.Library.controller.impl;

import edu.ada.service.Library.controller.AuthenticationWS;
import edu.ada.service.Library.model.dto.RegistrationModel;
import edu.ada.service.Library.model.entity.UserEntity;
import edu.ada.service.Library.service.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/auth")
public class AuthenticationWSimpl implements AuthenticationWS {

    protected static Logger log =LoggerFactory.getLogger(AuthenticationWSimpl.class);

    @Autowired
    private AuthService authService;


    /*IF USER SUCCESSFULLY LOGGED IN He/she will get TOKEN printed on postman
     (As a token i created class which generates random alphanumeric string with length of 30)
     */
    @Override
    @RequestMapping(value="/login",method = RequestMethod.GET)
    public ResponseEntity login(
            @RequestHeader("email") String email,
            @RequestHeader("password") String password
    ) {
       UserEntity result = authService.login(email, password);
       if (result==null){
           return new ResponseEntity("User does not exist or you entered wrong password ",HttpStatus.NOT_FOUND);
       } else{
           return  new ResponseEntity(
                   "This is your token(you will use it for adding/droping books) >> "
                           +result.getToken(),HttpStatus.OK);
       }

    }


    @Override
    @RequestMapping(value="/register",method= RequestMethod.POST)
    public ResponseEntity register(
           @RequestBody RegistrationModel formDATA
    ) {
       try {
           UserEntity user = authService.registration(formDATA);
           return  new ResponseEntity(
                   "Welcome "+user.getFirstname() +
                           ". Please use auth/login Url in order to receive your Token"
                   ,HttpStatus.CREATED);
       }
       catch (Exception e){
           return ResponseEntity.unprocessableEntity().build();
       }
    }
}
