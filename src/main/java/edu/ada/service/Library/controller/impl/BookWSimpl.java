package edu.ada.service.Library.controller.impl;

import edu.ada.service.Library.controller.BookWs;
import edu.ada.service.Library.model.dto.BookModel;
import edu.ada.service.Library.model.entity.BookEntity;
import edu.ada.service.Library.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BookWSimpl implements BookWs {

    @Autowired
    BookService bookService;

    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSimpl.class);


    //BELONGS TO NEW ASSIGNMENT
    @Override
    @RequestMapping(value="/bookId/{bookId}",method = RequestMethod.GET)
    public ResponseEntity searchByBookId(
            @PathVariable Long bookId
    ) {
        try {
            BookModel booksById = bookService.searchById(bookId) ;
            if (booksById!=null) {
                return new ResponseEntity(booksById, HttpStatus.OK);

            }else return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);

        }catch (Exception e){

            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        }

    }
    /*********************************************************************************/

    @Override
    @RequestMapping(value="",method = RequestMethod.GET)
    public ResponseEntity showAllBooks() {
        try {
            List<BookEntity> allBooks = bookService.showAllBooks() ;
            return new ResponseEntity(allBooks,HttpStatus.OK);
        }catch (Exception e){

            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


    //You dont have to be authorized in order to INSERT new book or search BOOKS in Library
    //I will use TOKEN for only DROP/ADD book actions which require registered user logically

    @Override
    @RequestMapping(value="/newbook",method= RequestMethod.POST)
    public ResponseEntity register(
            @RequestBody BookModel bookModel
    ) {
        try {
            BookEntity bookEntity=bookService.addNewBook(bookModel);
            return  new ResponseEntity(
                    bookEntity.getBookName()+
                    ","+bookEntity.getCategory(), HttpStatus.CREATED
            );

        }catch (Exception e){
            return ResponseEntity.unprocessableEntity().build();
        }
    }

    /********************************************************************************************************/
    //FOLLOWING METHODS WILL BE TYPE OF SEARCHES OF BOOK and each combination will have DIFFERENT URL
    @Override
    @RequestMapping(value="/category",method = RequestMethod.GET)
    public ResponseEntity searchByCategory(@RequestHeader String category) {
        List<BookEntity> result=bookService.searchByCategory(category);
        if (result.isEmpty()){
            return new ResponseEntity("Sorry I cannot find books with that category",HttpStatus.NOT_FOUND);
        } else{
            return  new ResponseEntity(result,HttpStatus.OK);
        }
    }

    @Override
    @RequestMapping(value="/authorName",method = RequestMethod.GET)
    public ResponseEntity searchByAuthorName(@RequestHeader String authorName) {
        List<BookEntity> result=bookService.searchByAuthorName(authorName);
        if (result.isEmpty()){
            return new ResponseEntity("Sorry I cannot find books with that Author name",HttpStatus.NOT_FOUND);
        } else{
            return  new ResponseEntity(result,HttpStatus.OK);
        }
    }

    @Override
    @RequestMapping(value="/bookName",method = RequestMethod.GET)
    public ResponseEntity searchByBookName(@RequestHeader String bookName) {
        List<BookEntity> result=bookService.searchByBookName(bookName);
        if (result.isEmpty()){
            return new ResponseEntity("Sorry I cannot find books with that Book name",HttpStatus.NOT_FOUND);
        } else{
            return  new ResponseEntity(result,HttpStatus.OK);
        }
    }

    @Override
    @RequestMapping(value="/authorAndCategory",method = RequestMethod.GET)
    public ResponseEntity searchByCategoryAndAuthorName(
            @RequestHeader String category,
            @RequestHeader String authorName
    ) {
        List<BookEntity> result=bookService.searchByCategoryAndAuthorName(category,authorName);
        if (result.isEmpty()){
            return new ResponseEntity("Sorry I cannot find books with that search query",HttpStatus.NOT_FOUND);
        } else{
            return  new ResponseEntity(result,HttpStatus.OK);
        }
    }

    @Override
    @RequestMapping(value="/available",method = RequestMethod.GET)
    public ResponseEntity searchByAvailable(@RequestHeader Boolean available) {
        List<BookEntity> result=bookService.searchByAvailable(available);
        if (result.isEmpty()){
            return new ResponseEntity("Sorry There is no available book",HttpStatus.NOT_FOUND);
        } else{
            return  new ResponseEntity(result,HttpStatus.OK);
        }
    }


}
