package edu.ada.service.Library.controller.impl;

import edu.ada.service.Library.controller.CommentWs;
import edu.ada.service.Library.model.dto.BookModel;
import edu.ada.service.Library.model.entity.UserEntity;
import edu.ada.service.Library.service.AuthService;
import edu.ada.service.Library.service.BookService;
import edu.ada.service.Library.service.CommentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/comments")
public class CommentWsimpl implements CommentWs {

    @Autowired
    BookService bookService;

    @Autowired
    CommentService commentService;

    @Autowired
    AuthService authService;


    protected static Logger log = LoggerFactory.getLogger(AuthenticationWSimpl.class);


    @Override
    @RequestMapping(value = "/add-NewComment/{bookExtId}", method= RequestMethod.POST)
    public ResponseEntity addComment(
            @RequestHeader String token,
            @PathVariable Long bookExtId,
            @RequestBody String content) {
        try {
            BookModel requestedBook=bookService.searchById(bookExtId);
            UserEntity author= authService.getUserByToken(token);

            commentService.addComment(author.getFirstname(),requestedBook.getId(),content);
            return new ResponseEntity(HttpStatus.OK);
        }catch (Exception e){
            return  new ResponseEntity(HttpStatus.NOT_FOUND);

        }
    }


    @Override
    @RequestMapping(value = "/replyToComment/{cmmntID}", method= RequestMethod.POST)
    public ResponseEntity replyToComment(
            @RequestHeader String token,
            @PathVariable String cmmntID,
            @RequestBody String content) {
        try {
            UserEntity author =authService.getUserByToken(token);
            commentService.replyToComment(author.getFirstname(),cmmntID,content);
            return new ResponseEntity(HttpStatus.OK);

        }catch (Exception e){

            return  new ResponseEntity(HttpStatus.NOT_FOUND);

        }
    }
}
