package edu.ada.service.Library.controller;

import edu.ada.service.Library.model.dto.BookModel;
import edu.ada.service.Library.model.entity.BookEntity;
import org.springframework.http.ResponseEntity;

import java.util.List;


public interface BookWs {

    ResponseEntity register(BookModel bookModel);

    ResponseEntity searchByCategory(String category);
    ResponseEntity searchByAuthorName(String authorName);
    ResponseEntity searchByBookName(String bookName);
    ResponseEntity searchByCategoryAndAuthorName(String category,String authorName);
    ResponseEntity searchByAvailable(Boolean available);
    ResponseEntity showAllBooks();
    ResponseEntity searchByBookId(Long bookId);

}
