package edu.ada.service.Library.model.dto;


import edu.ada.service.Library.model.entity.BookEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
public class BookModel implements Serializable {

    private Long id;
    private String bookName;
    private String authorName;
    private String authorSurname;
    private String category;
    private Boolean available;
    private List<CommentModel> comments;


    public BookModel(BookEntity output) {

        this.id=output.getBookId();
        this.bookName=output.getBookName();
        this.authorName=output.getAuthorName();
        this.authorSurname=output.getAuthorSurname();
        this.category=output.getCategory();
        this.available=output.getAvailable();

    }


    public void setComments(List<CommentModel> comments) {
        this.comments = comments;
    }

}
