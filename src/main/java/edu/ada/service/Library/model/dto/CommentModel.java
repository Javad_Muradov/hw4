package edu.ada.service.Library.model.dto;

import edu.ada.service.Library.model.entity.CommentEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
public class CommentModel implements Serializable {

    private String id;
    private String  author_name;
    private String  content;
    private List<CommentEntity> replies;

    public CommentModel(CommentEntity entity){
        this.id=entity.getId();
        this.author_name=entity.getCmntAuthorName();
        this.content=entity.getCmntContent();
        this.replies = entity.getReplies();
    }
}
