package edu.ada.service.Library.model.entity;

import edu.ada.service.Library.model.dto.CommentModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Data
@Document
public class CommentEntity {
    @Id
    private String id;

    private Long bookExtId;

    private String cmntAuthorName;

    private String cmntContent;

    @OneToMany
    private List<CommentEntity> replies;

    public CommentEntity(String authorName, Long bookExtId, String content) {
        this.cmntAuthorName=authorName;
        this.bookExtId=bookExtId;
        this.cmntContent=content;
    }

}
