package edu.ada.service.Library.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity(name="ProfileEntity")
@Table(name="addedBooks")
public class AddedBooksEntity {

    @Id
    @SequenceGenerator(
            name = "rent_sequence",
            sequenceName = "rent_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "rent_sequence"
    )
    private Long rentID;

    @ManyToOne(targetEntity = UserEntity.class)
    @JoinColumn
    @JsonIgnore
    private UserEntity owner;

    @ManyToOne(targetEntity = BookEntity.class)
    @JoinColumn
    private BookEntity added;

    public AddedBooksEntity() {
    }

    public AddedBooksEntity( UserEntity owner, BookEntity added) {
        this.owner = owner;
        this.added = added;
    }


    public Long getRentID() {
        return rentID;
    }

    public void setRentID(Long rentID) {
        this.rentID = rentID;
    }

    public UserEntity getOwner() {
        return owner;
    }

    public void setOwner(UserEntity owner) {
        owner = owner;
    }

    public BookEntity getAdded() {
        return added;
    }

    public void setAdded(BookEntity added) {
        this.added = added;
    }

    @Override
    public String toString() {
        return "AddedBooksEntity{" +
                "rentID=" + rentID +
                ", Owner=" + owner +
                ", added=" + added +
                '}';
    }
}
