package edu.ada.service.Library.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import edu.ada.service.Library.model.dto.RegistrationModel;

import javax.persistence.*;
import java.util.List;

@Entity(name="UserEntity")
@Table(name="users")
public class UserEntity {
    @Id
    @SequenceGenerator(
            name = "user_sequence",
            sequenceName = "user_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "user_sequence"
    )

    private Long user_id;
    private String firstname;
    private String lastname;

    @OneToMany(targetEntity = AddedBooksEntity.class, mappedBy = "owner", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<AddedBooksEntity> addedBooks;


    @Column(
            nullable = false,
            unique = true
    )
    private String email;

    @Column(
            nullable = false
    )
    private String password;
    private String token;


    public  UserEntity(){

    }
    public UserEntity(RegistrationModel registrationModel) {
        this.firstname = registrationModel.getFirstname();
        this.lastname = registrationModel.getLastname();
        this.email = registrationModel.getEmail();
        this.password = registrationModel.getPassword();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "user_id=" + user_id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
