package edu.ada.service.Library.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import edu.ada.service.Library.model.dto.BookModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity(name="BookEntity")
@Table(name="Books")
public class BookEntity {

    @Id
    @SequenceGenerator(
            name = "book_sequence",
            sequenceName = "book_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "book_sequence"
    )

    private Long bookId;

    @Column(
            unique = true
    )
    private String bookName;
    private String authorName;
    private String authorSurname;
    private String category;
    private Boolean available;

    @OneToMany(targetEntity = AddedBooksEntity.class, mappedBy = "added", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<AddedBooksEntity> addedBooks;

    public BookEntity() {
    }

    public BookEntity(BookModel bookModel) {
        this.bookName = bookModel.getBookName();
        this.authorName = bookModel.getAuthorName();
        this.authorSurname = bookModel.getAuthorSurname();
        this.category = bookModel.getCategory();
        this.available = bookModel.getAvailable();
    }

}
